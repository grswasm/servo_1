#include <Servo.h> 
 
Servo myservo;  
int pos = 0;    
int delta = 1;

void setup() 
{ 
  myservo.attach(9);  
  myservo.write(0);   
  delay(15);   
} 
 
void loop() 
{ 
  myservo.write(pos);
  delay(15);
  if (pos == 179)
   delta= -1;         
  else if (pos == 0)
    delta = 1;
    pos = pos + delta;
}
